package id.ac.ui.cs.tutorial4.service;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import id.ac.ui.cs.tutorial4.model.Conference;
import id.ac.ui.cs.tutorial4.repository.ConferenceRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ConverenceServiceImpl
 */
@Service
public class ConferenceServiceImpl implements ConferenceService {

    @Autowired
    ConferenceRepo conferenceRepo;

    @Override
    public List<Conference> getAllLatestConference() {
        return conferenceRepo.findAllByOrderByIdAsc();
    }

    @Override
    public void addConference(Conference conference) {
        conferenceRepo.save(conference);
    }

    @Override
    public void removeConferenceById(Long id) {
        conferenceRepo.deleteById(id);
    }

    @Override
    public Conference getConferenceById(Long id) {
        if (!conferenceRepo.existsById(id)) {
            throw new EntityNotFoundException();
        }
        return conferenceRepo.getOne(id);
    }

    @Override
    public void updateConference(Conference conference) {
        if (!conferenceRepo.existsById(conference.getId())) {
            throw new EntityNotFoundException();
        }
        conferenceRepo.save(conference);
    }
}