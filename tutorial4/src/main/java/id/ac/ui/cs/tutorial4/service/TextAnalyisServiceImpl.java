package id.ac.ui.cs.tutorial4.service;

import id.ac.ui.cs.tutorial4.client.RestClientSingleton;
import id.ac.ui.cs.tutorial4.dataclass.Documents;
import id.ac.ui.cs.tutorial4.dataclass.KeyPhrasesResult;

import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * TextAnalyisServiceImpl
 * This class will implement the TextAnalyisService
 * Also acts as a client for the text analyis on microsoft for convenience.
 */
@Service
public class TextAnalyisServiceImpl implements TextAnalysisService {
    
    public static final String KEY_PHRASES_ANALYSIS_ENDPOINT = "https://westus.api.cognitive.microsoft.com/text/analytics/v2.1/keyPhrases";

    @Override
    public KeyPhrasesResult extractKeyPhrases(Documents documents) {
        RestTemplate restTemplate = RestClientSingleton.getTemplate();
        HttpEntity<Documents> request = new HttpEntity<>(documents, createHeaders());
        return restTemplate.postForObject(KEY_PHRASES_ANALYSIS_ENDPOINT, request, KeyPhrasesResult.class);
    }


    /**
     * Will give you the custom headers required to use the microsoft service
     * Adapted from this https://stackoverflow.com/questions/21723183/spring-resttemplate-to-post-request-with-custom-headers-and-a-request-object
     * @return
     */
    private MultiValueMap<String, String> createHeaders() {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

        headers.add("Ocp-Apim-Subscription-Key", "30ac98f6a2394a4297ca98beb0b87c4d");
        headers.add("Content-Type", "application/json");
        return headers;
    }
    
}