package id.ac.ui.cs.tutorial4.controller;

import java.sql.Date;

import javax.persistence.EntityNotFoundException;

import id.ac.ui.cs.tutorial4.dataclass.PaperResult;
import id.ac.ui.cs.tutorial4.model.Conference;
import id.ac.ui.cs.tutorial4.repository.ConferenceRepo;
import id.ac.ui.cs.tutorial4.service.ConferenceService;
import id.ac.ui.cs.tutorial4.service.PaperFinderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class HomeController {

    private static final String CONFERENCE = "conference";
    private static final String ERROR_MSG = "error_msg";
    private static final String REDIRECT = "redirect:/";
    private static final String REDIRECT_GOOF = "redirect:/goof";

    @Autowired
    PaperFinderService paperFinderService;

    @Autowired
    ConferenceService conferenceService;

    @Autowired
    ConferenceRepo conferenceRepo; // testing purposes for testdb only. delete later.

    @GetMapping("/")
    @PreAuthorize("isAuthenticated()")
    public String home(Model model) {
        model.addAttribute("conferenceList", conferenceService.getAllLatestConference());
        return "_home";
    }

    @GetMapping("/add-conference")
    @PreAuthorize("isAuthenticated()")
    public String addConferenceForm(Model model) {
        model.addAttribute(CONFERENCE, new Conference());
        return "add-conference";
    }

    @PostMapping("/add-conference")
    @PreAuthorize("isAuthenticated()")
    public String addConferenceSubmit(@ModelAttribute Conference newConference, BindingResult bindingResult,
            RedirectAttributes redAttr) {
        if (bindingResult.hasErrors()) {
            redAttr.addFlashAttribute(ERROR_MSG, "Terdapat kesalahan pada form ada.");
            return REDIRECT_GOOF;
        }
        conferenceRepo.save(newConference);
        return REDIRECT;
    }

    @GetMapping("/update/{id}")
    @PreAuthorize("isAuthenticated()")
    public String updateConferenceForm(@PathVariable("id") long id, RedirectAttributes redirectAttributes,
            Model model) {

        try {
            model.addAttribute(CONFERENCE, conferenceService.getConferenceById(id));
        } catch (EntityNotFoundException e) {
            redirectAttributes.addFlashAttribute(ERROR_MSG, "Conference not found.");
            return REDIRECT_GOOF;
        }
        return "update-conference";
    }

    @PostMapping("/update-conference")
    @PreAuthorize("isAuthenticated()")
    public String updateConferenceSubmit(@ModelAttribute Conference conference, RedirectAttributes redirectAttributes) {
        try {
            conferenceService.updateConference(conference);
        } catch (EntityNotFoundException e) {
            redirectAttributes.addFlashAttribute(ERROR_MSG, "The conference that you want to update doesn't exist.");
            return REDIRECT_GOOF;
        }
        return REDIRECT;
    }

    @PostMapping("/delete/{id}")
    @PreAuthorize("isAuthenticated()")
    public String deleteConference(@PathVariable("id") long id) {
        conferenceService.removeConferenceById(id);
        return REDIRECT;
    }

    @GetMapping("/view/{id}")
    @PreAuthorize("isAuthenticated()")
    public String viewConference(@PathVariable("id") long id, Model model, RedirectAttributes redAttr) {
        try {
            Conference conference = conferenceService.getConferenceById(id);
            model.addAttribute(CONFERENCE, conference);
            PaperResult pResult = paperFinderService.findPaperFromConference(conference);

            if (!pResult.isError()) {
                model.addAttribute("paperList", pResult.getPaperList());
            }
            return "conference-detail";
        } catch (EntityNotFoundException e) {
            redAttr.addFlashAttribute(ERROR_MSG, "Conference not found.");
            return REDIRECT_GOOF;
        }
    }

    @GetMapping("/testdb")
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public String home() {
        Conference testConference = new Conference();
        testConference.setNama("Cloud");
        testConference.setAbstrak("Computing");
        testConference.setRuanganDipakai("123");
        testConference.setTanggalMulai(new Date(0));

        if (conferenceRepo.findByNama("Cloud").isEmpty()) {
            return "Yep, there's already one in the database. It's working.";
        } else {
            conferenceRepo.save(testConference);
        }

        return "If you're seeing this, then most likely the test is finished smoothly";
    }

    @GetMapping("/goof")
    @PreAuthorize("isAuthenticated()")
    public String customErrorPage(Model model) {
        return "error/custom_error";
    }
}