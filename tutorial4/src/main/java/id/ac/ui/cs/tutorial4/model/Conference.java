package id.ac.ui.cs.tutorial4.model;

import java.sql.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Conference model.
 */
@Entity
@Table(name = "conference")
public class Conference {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nama")
    private String nama;

    @NotNull
    @Column(name = "tanggal_mulai")
    private Date tanggalMulai;

    @NotNull
    @Column(name = "ruangan_dipakai")
    private String ruanganDipakai;

    @NotNull
    @Column(name = "abstrak")
    private String abstrak;
    
    public Conference() {
        // Do something
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Date getTanggalMulai() {
        return this.tanggalMulai;
    }

    public void setTanggalMulai(Date tanggalMulai) {
        this.tanggalMulai = tanggalMulai;
    }

    public String getRuanganDipakai() {
        return this.ruanganDipakai;
    }

    public void setRuanganDipakai(String string) {
        this.ruanganDipakai = string;
    }

    public String getAbstrak() {
        return this.abstrak;
    }

    public void setAbstrak(String abstrak) {
        this.abstrak = abstrak;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Conference)) {
            return false;
        }
        Conference conference = (Conference) o;
        return Objects.equals(id, conference.id) && Objects.equals(nama, conference.nama) && Objects.equals(tanggalMulai, conference.tanggalMulai) && ruanganDipakai == conference.ruanganDipakai && Objects.equals(abstrak, conference.abstrak);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nama, tanggalMulai, ruanganDipakai, abstrak);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", nama='" + getNama() + "'" +
            ", tanggalMulai='" + getTanggalMulai() + "'" +
            ", ruanganDipakai='" + getRuanganDipakai() + "'" +
            ", abstrak='" + getAbstrak() + "'" +
            "}";
    }
}