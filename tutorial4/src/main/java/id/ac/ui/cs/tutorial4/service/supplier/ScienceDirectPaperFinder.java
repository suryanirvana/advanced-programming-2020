package id.ac.ui.cs.tutorial4.service.supplier;

import id.ac.ui.cs.tutorial4.dataclass.Paper;
import id.ac.ui.cs.tutorial4.dataclass.PaperResult;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;

import java.io.IOException;
import java.util.*;

public class ScienceDirectPaperFinder {
    private static ScienceDirectPaperFinder ourInstance = new ScienceDirectPaperFinder();
    private static WebClient webClient;
    private static HtmlPage pageResponse;
    private static final String SCIENCE_DIRECT_URL = "https://www.sciencedirect.com/";
    private static boolean isInitialize = false;

    public static ScienceDirectPaperFinder getInstance() {
        System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "fatal");
        return ourInstance;
    }

    private ScienceDirectPaperFinder() {

    }

    private void initialize() throws IOException {
        webClient = new WebClient(BrowserVersion.CHROME);
        webClient.getOptions().setJavaScriptEnabled(false);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        pageResponse = webClient.getPage(SCIENCE_DIRECT_URL);
        isInitialize = true;
    }

    private List<HtmlDivision> getResponseData(String keyword) throws IOException {
        HtmlTextInput keywordsInput = getScienceDirectHomePage().getElementByName("qs");
        keywordsInput.setValueAttribute(keyword);

        HtmlButton input = getScienceDirectHomePage()
                .getFirstByXPath("//*[@id=\"aa-srp-search-submit-button\"]/button");

        HtmlPage newPage = input.click();

        return newPage.getByXPath("//div[@class='result-item-content']");
    }

    private HtmlPage getScienceDirectHomePage() throws IOException {
        if (!isInitialize) {
            initialize();
        }
        return pageResponse;
    }

    private Map<String, String> getReferenceLinkData(List<HtmlDivision> responseData) {
        Map<String, String> paperData = new HashMap<>();
        System.err.println(responseData.size());
        for (HtmlDivision data : responseData) {
            for (DomElement header2Element : data.getChildElements()) {
                if (header2Element.getNodeName().equalsIgnoreCase("h2")) {
                    header2Element.getFirstElementChild();
                    String url = header2Element.getFirstElementChild().getFirstElementChild().getAttribute("href");
                    paperData.put(header2Element.asText(), SCIENCE_DIRECT_URL + url);
                }
            }
        }
        return paperData;
    }

    private Paper createPaperInstance(String tittle, String url) throws IOException {
        return new Paper(tittle, url, getAbstract(url));
    }

    private String getAbstract(String url) throws IOException {
        HtmlPage paperDetailPage = webClient.getPage(url);

        System.out.println(url);
        HtmlDivision abstractData = paperDetailPage.getFirstByXPath("//div[@class='abstract author']");
        System.out.println("Checking URL");
        System.out.println(abstractData.getChildElementCount());
        Iterator<DomElement> abstrakDataContent = abstractData.getChildElements().iterator();
        String abstrakData = "";

        while (abstrakDataContent.hasNext()) {
            DomElement content = abstrakDataContent.next();
            if (!content.asText().equalsIgnoreCase("Abstract")) {
                abstrakData = content.asText();
            }
        }
        return abstrakData;
    }

    private List<Paper> getAllPaperInPage(String keyword) throws IOException {
        List<HtmlDivision> pageResponseData = getResponseData(keyword);
        Map<String, String> paperInitialData = getReferenceLinkData(pageResponseData);
        List<Paper> allPaperInPage = new ArrayList<>();



        for (String paperTitle : paperInitialData.keySet()) {
            System.out.println("CHECKING SOMETHING");
            System.out.println(paperTitle);
            System.out.println(paperInitialData.get(paperTitle));
            allPaperInPage.add(createPaperInstance(paperTitle, paperInitialData.get(paperTitle)));
        }
        return allPaperInPage;
    }

    public PaperResult findPapers(String keyword) {
        PaperResult paperWithKeyWords = new PaperResult();
        try {
            List<Paper> paperDataFromWeb = getAllPaperInPage(keyword);
            paperWithKeyWords.setPaperList(paperDataFromWeb);
            paperWithKeyWords.setError(false);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            paperWithKeyWords.setError(true);
        }
        return paperWithKeyWords;
    }
}
