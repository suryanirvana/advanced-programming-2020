package id.ac.ui.cs.tutorial4.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.ac.ui.cs.tutorial4.dataclass.Document;
import id.ac.ui.cs.tutorial4.dataclass.Documents;
import id.ac.ui.cs.tutorial4.dataclass.KeyPhrasesResult;
import id.ac.ui.cs.tutorial4.service.TextAnalysisService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * CheckController
 * This controller's main objective is to make sure
 * that the service is actually usable and not 
 */
@RestController
@RequestMapping("api")
public class APIController {

    @Autowired
    TextAnalysisService textAnalysisService;

    @GetMapping()
    @PreAuthorize("permitAll()")
    @ResponseBody
    public Map<String, Serializable> information() {
        Map<String, Serializable> information = new HashMap<>();
        List<String> resourceList = new ArrayList<>();
        resourceList.add("Resource number one");
        resourceList.add("Resource number two");
        information.put("resources", (Serializable) resourceList);
        return information;
    }

    @GetMapping("test-text-analysis")
    @PreAuthorize("permitAll()")
    @ResponseBody
    public KeyPhrasesResult testTextAnaylsis(@RequestParam(name = "language", required = true) String language,
            @RequestParam(name = "text", required = true) String text) {
        Documents docs = new Documents();
        docs.addDocument(new Document("", language, text));
        return textAnalysisService.extractKeyPhrases(docs);
    }
}