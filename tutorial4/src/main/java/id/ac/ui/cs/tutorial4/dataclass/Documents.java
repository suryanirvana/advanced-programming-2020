package id.ac.ui.cs.tutorial4.dataclass;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Documents
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Documents {

    @JsonProperty("documents")
    List<Document> documents = new ArrayList<>();    

    public Documents() {
    }

    public Documents(List<Document> documents) {
        this.documents = documents;
    }

    public List<Document> getDocuments() {
        return this.documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public void addDocument(Document document) {
        document.setId(String.valueOf(documents.size() + 1));
        documents.add(document);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Documents)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(documents);
    }

    @Override
    public String toString() {
        return "{" +
            " documents='" + getDocuments() + "'" +
            "}";
    }
}