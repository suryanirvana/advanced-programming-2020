package id.ac.ui.cs.tutorial4.dataclass;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * KeyPhrasesResult
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class KeyPhrasesResult {

    @JsonProperty("documents")
    List<KeyPhrasesResponseDocument> documents = Collections.synchronizedList(new ArrayList<KeyPhrasesResponseDocument>());

    @JsonProperty("error")
    List<String> error = Collections.synchronizedList(new ArrayList<String>());

    public KeyPhrasesResult() {
    }

    public KeyPhrasesResult(List<KeyPhrasesResponseDocument> documents, List<String> error) {
        this.documents = documents;
        this.error = error;
    }

    public List<KeyPhrasesResponseDocument> getDocuments() {
        return this.documents;
    }

    public void setDocuments(List<KeyPhrasesResponseDocument> documents) {
        this.documents = documents;
    }

    public List<String> getError() {
        return this.error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof KeyPhrasesResult)) {
            return false;
        }
        KeyPhrasesResult keyPhrasesResult = (KeyPhrasesResult) o;
        return Objects.equals(documents, keyPhrasesResult.documents) && Objects.equals(error, keyPhrasesResult.error);
    }

    @Override
    public int hashCode() {
        return Objects.hash(documents, error);
    }

    @Override
    public String toString() {
        return "{" +
            " documents='" + getDocuments() + "'" +
            ", error='" + getError() + "'" +
            "}";
    }
}