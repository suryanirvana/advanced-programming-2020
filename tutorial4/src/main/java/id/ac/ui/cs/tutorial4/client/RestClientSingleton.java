package id.ac.ui.cs.tutorial4.client;

import org.springframework.web.client.RestTemplate;

/**
 * RestClientSingleton
 * This class utilizes the singleton pattern
 * so that the RestTemplate object can't be
 * initialized more than once.
 * 
 * It will immediately initialized as soon as
 * the program is up and running. This removed the
 * condition where two thread that's accessing the same
 * object 
 */
public class RestClientSingleton {

    // Initialized even if not used
    // Save for multi-threading aka Spring Controller
    private static RestTemplate restTemplate = new RestTemplate();

    // Empty constructor
    private RestClientSingleton() {
        // Do something
    }

    /**
     * Will give the REST template.
     * @return
     */
    public static RestTemplate getTemplate() {
        return restTemplate;
    }
}