package id.ac.ui.cs.advprog.tutorial7.observer.repository;

import id.ac.ui.cs.advprog.tutorial7.observer.core.MagicResearch;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class MagicResearchRepository {
        private Map<String, MagicResearch> magicResearch = new HashMap<>();

        public Map<String, MagicResearch> getMagicResearchs() {
                return magicResearch;
        }

        public MagicResearch save(MagicResearch savedMagicResearch) {
                MagicResearch existingMagicResearch = magicResearch.get(savedMagicResearch.getTitle());
                if (existingMagicResearch == null) {
                        magicResearch.put(savedMagicResearch.getTitle(), savedMagicResearch);
                        return savedMagicResearch;
                } else {
                        return null;
                }
        }
}
