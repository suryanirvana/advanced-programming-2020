package id.ac.ui.cs.advprog.tutorial7.strategy.service;

import id.ac.ui.cs.advprog.tutorial7.strategy.core.AttackAction;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.Harvest;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.MagicMissile;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.DefenseAction;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.Barrier;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.Counter;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.SupportAction;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.Enhance;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.Regenerate;

import id.ac.ui.cs.advprog.tutorial7.strategy.repository.ActionRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActionServiceImpl  implements ActionService{

    private ActionRepository actionRepo;

    public ActionServiceImpl(ActionRepository actionRepo){
        this.actionRepo = actionRepo;
        actionRepo.addAttackAction(new Harvest());
        actionRepo.addAttackAction(new MagicMissile());
        actionRepo.addDefenseAction(new Barrier());
        actionRepo.addDefenseAction(new Counter());
        actionRepo.addSupportAction(new Enhance());
        actionRepo.addSupportAction(new Regenerate());
    }

    public ActionServiceImpl(){
        this(new ActionRepository());
    }

    public List<AttackAction> getAttackActions(){
        return actionRepo.getAttackActions();
    }

    public List<DefenseAction> getDefenseActions(){
        return actionRepo.getDefenseActions();
    }

    public List<SupportAction> getSupportActions(){
        return actionRepo.getSupportActions();
    }
}