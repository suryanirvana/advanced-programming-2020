package id.ac.ui.cs.advprog.tutorial7.observer.service;

import id.ac.ui.cs.advprog.tutorial7.observer.core.Researcher;
import id.ac.ui.cs.advprog.tutorial7.observer.core.MagicResearch;

import java.util.List;

public interface MagicAssociationService {
        void addResearch(MagicResearch magicResearch);
        List<Researcher> getResearchers();
}
