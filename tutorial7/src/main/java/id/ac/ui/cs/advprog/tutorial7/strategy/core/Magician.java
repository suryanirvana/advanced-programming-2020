package id.ac.ui.cs.advprog.tutorial7.strategy.core;

public class Magician {
	
	private String name;
	private AttackAction attackAction;
	private DefenseAction defenseAction;
	private SupportAction supportAction;
	
	// TO DO: The constructor still needs additional parameter. Add those needed parameters
	public Magician(String name){
		this.name = name;
		// TO DO : initialization for other parameters
	}
	
	// TO DO : Change the implementation so it can get magician's attack action
	// Hint : Finish the first task (add additional constructor parameter) first
	public AttackAction getAttackAction(){
		return new DummyAction();
	}
	
	// TO DO : Implement changing AttackAction
	// Hint : Finish the first task (add additional constructor parameter) first
	public void setAttackAction(AttackAction param){
		
	}
	
	// TO DO : Change the implementation so it can get magician's defense action
	// Hint : Finish the first task (add additional constructor parameter) first
	public DefenseAction getDefenseAction(){
		return new DummyAction();
	}
	
	// TO DO : Implement changing DefenseAction
	// Hint : Finish the first task (add additional constructor parameter) first
	public void setDefenseAction(DefenseAction param){
		
	}
	
	// TO DO : Change the implementation so it can get magician's support action
	// Hint : Finish the first task (add additional constructor parameter) first
	public SupportAction getSupportAction(){
		return new DummyAction();
	}
	
	// TO DO : Implement changing SupportAction
	// Hint : Finish the first task (add additional constructor parameter) first 
	public void setSupportAction(SupportAction param){
		
	}
	
	// TO DO : Implement this method
	public String attack(){
		return "Implement me";
	}
	
	// TO DO : Implement this method
	public String defense(){
		return "Implement me";
	}
	
	// TO DO : Implement this method
	public String support(){
		return "Implement me";
	}
	
	public String getName(){
		return name;
	}


}