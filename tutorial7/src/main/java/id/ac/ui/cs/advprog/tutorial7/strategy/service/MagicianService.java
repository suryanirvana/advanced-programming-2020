package id.ac.ui.cs.advprog.tutorial7.strategy.service;

import id.ac.ui.cs.advprog.tutorial7.strategy.core.Magician;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.AttackAction;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.DefenseAction;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.SupportAction;

import java.util.Map;

public interface MagicianService {
    Map<String, Magician> getMagicians();
    Magician getMagicianByName(String name);
    Magician changeActions(Magician magician, AttackAction attackAction, DefenseAction defenseAction, SupportAction supportAction);
    Magician changeActions(String magicianName, AttackAction attackAction, DefenseAction defenseAction, SupportAction supportAction);
}