package id.ac.ui.cs.advprog.tutorial7.strategy.service;

import id.ac.ui.cs.advprog.tutorial7.strategy.core.Magician;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.AntiMagician;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.DeathMagician;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.Enhancer;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.Spellcaster;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.AttackAction;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.DefenseAction;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.SupportAction;
import id.ac.ui.cs.advprog.tutorial7.strategy.repository.MagicianRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class MagicianServiceImpl  implements MagicianService {

    private MagicianRepository magicianRepo;

    public MagicianServiceImpl(MagicianRepository magicianRepo){
        this.magicianRepo = magicianRepo;
        Magician[] initMages = {
            new AntiMagician("Kerry"),
            new DeathMagician("Chrome"),
            new Enhancer("Shirou"),
            new Spellcaster("Magilou")
        };

        for(int i = 0; i < initMages.length; i++){
            magicianRepo.add(initMages[i]);
        }
    }

    public MagicianServiceImpl(){
        this(new MagicianRepository());
    }

    public Map<String, Magician> getMagicians(){
        return magicianRepo.getMagicians();
    }

    public Magician getMagicianByName(String name){
        return magicianRepo.getMagicianByName(name);
    }

    public Magician changeActions(Magician magician, AttackAction attackAction, DefenseAction defenseAction, SupportAction supportAction){
        magician.setAttackAction(attackAction);
        magician.setDefenseAction(defenseAction);
        magician.setSupportAction(supportAction);
        return magician;
    }
    public Magician updateMagician(Magician  magician) {
        magicianRepo.add(magician);
        return magician;
    }



    public Magician changeActions(String name, AttackAction attackAction, DefenseAction defenseAction, SupportAction supportAction){
        return changeActions(getMagicianByName(name), attackAction, defenseAction, supportAction);
    }
}