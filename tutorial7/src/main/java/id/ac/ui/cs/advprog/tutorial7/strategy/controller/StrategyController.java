package id.ac.ui.cs.advprog.tutorial7.strategy.controller;

import id.ac.ui.cs.advprog.tutorial7.strategy.core.Magician;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.AttackAction;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.DefenseAction;
import id.ac.ui.cs.advprog.tutorial7.strategy.core.SupportAction;

import id.ac.ui.cs.advprog.tutorial7.strategy.service.MagicianService;
import id.ac.ui.cs.advprog.tutorial7.strategy.service.ActionService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class StrategyController {

    @Autowired
    private ActionService actionService;

    @Autowired
    private MagicianService magicianService;

    @GetMapping("/magicians")
    public String getMagicians(Model model){
        model.addAttribute("magicians", magicianService.getMagicians()); // both untuk list of magicians sm untuk form ganti skill
        model.addAttribute("attackActions", actionService.getAttackActions()); // untuk form ganti skill
        model.addAttribute("defenseActions", actionService.getDefenseActions()); // untuk form ganti skill
        model.addAttribute("supportActions", actionService.getSupportActions()); // untuk form ganti skill

        return "strategy/magicians";
    }

    @PostMapping("/changeActions")
    public String changeActionsPost(@ModelAttribute("magician-selected") Magician magician
    ){
        return "redirect:/magicians";
    }
}