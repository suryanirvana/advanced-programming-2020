package id.ac.ui.cs.advprog.tutorial7.strategy.repository;

import org.springframework.stereotype.Repository;

import id.ac.ui.cs.advprog.tutorial7.strategy.core.Magician;

import java.util.Map;
import java.util.HashMap;

@Repository
public class MagicianRepository {
    
    // The key is Magician.name()
    private Map<String, Magician> magicianMap = new HashMap<>();

    public Map<String, Magician> getMagicians(){
        return magicianMap;
    }

    public Magician getMagicianByName(String name){
        return magicianMap.get(name);
    }

    public Magician add(Magician magician){
        String name = magician.getName();
        magicianMap.put(name, magician);
        return magician;
    }
}
