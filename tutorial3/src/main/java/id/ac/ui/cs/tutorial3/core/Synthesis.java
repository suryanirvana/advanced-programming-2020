package id.ac.ui.cs.tutorial3.core;

public abstract class Synthesis {
	protected int manaGain;
	protected String name;
	protected int mana;

	public Synthesis(String name, int manaGain, int mana){
		this.manaGain = manaGain;
		this.name = name;
		this.mana = mana;
	}

	public int getManaGain(){
		return this.manaGain;
	}

	public int getMana(){
		return this.mana;
	}

	public String getName(){
		return this.name;
	}

	public void requestMana(Manalith manalith){
		manalith.addRequest(this);
	}

	@Override
	public String toString(){
		return "Name : " + this.name + ", Mana = " + this.mana;
	}

	public abstract boolean respond();
	public abstract String requestMessage();
}