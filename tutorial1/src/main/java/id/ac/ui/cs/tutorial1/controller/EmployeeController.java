package id.ac.ui.cs.tutorial1.controller;

import id.ac.ui.cs.tutorial1.service.GuildEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;


@Controller
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    GuildEmployeeService guildEmployeeService;

    @GetMapping(value = "/")
    public String home(Model model) {
        model.addAttribute("employee", guildEmployeeService.findAll());
        return "home";
    }

    @GetMapping(value = "/add")
    public String addEmployeePage() {
        return "add-employee";
    }

    @PostMapping(value = "/add")
    public ModelAndView addEmployeeToSystem(HttpServletRequest request) {
        guildEmployeeService.addEmployee(request.getParameter("employeeName"), request.getParameter("familyName"),
                request.getParameter("birthDate"), request.getParameter("type"));
        return new ModelAndView( "redirect:/employee/");
    }



}
