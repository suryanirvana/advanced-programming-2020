package id.ac.ui.cs.tutorial5.service;

import id.ac.ui.cs.tutorial5.core.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class CraftServiceImpl implements CraftService {


    private static final String DRAGON = "Dragon Breath";
    private static final String RHAPSODY = "Void Rhapsody";
    private static final String SYMPHONY = "Determination Symphony";
    private static final String OPERA = "Opera of Wasteland";
    private static final String FIRE = "FIRE BIRD";
    private String[] itemName = {DRAGON, RHAPSODY, SYMPHONY, OPERA, FIRE};
    private List<CraftItem> allItems = new ArrayList<>();

    @Override
    public CraftItem createItem(String itemName) {
        CraftItem craftItem;
        switch (itemName) {
            case DRAGON:
                craftItem = createDragonBreath();
                break;
            case RHAPSODY:
                craftItem = createVoidRhapsody();
                break;
            case SYMPHONY:
                craftItem = createDeterminationSymphony();
                break;
            case OPERA:
                craftItem = createOperaOfWasteland();
                break;
            case FIRE:
                craftItem = createFireBird();
                break;
            default:
                craftItem = createFireBird();
        }
        craftItem.composeRecipes();
        allItems.add(craftItem);
        return craftItem;
    }

    private CraftItem createDragonBreath() {
        CraftItem dragonBreath = new CraftItem(DRAGON);
        CompletableFuture.runAsync(() -> dragonBreath.addRecipes(new SilentField())).thenRun(() -> dragonBreath.addRecipes(new FireCrystal())).thenRun(dragonBreath::composeRecipes);
        return dragonBreath;
    }

    private CraftItem createVoidRhapsody() {
        CraftItem voidRhapsody = new CraftItem(RHAPSODY);
        CompletableFuture.runAsync(() -> voidRhapsody.addRecipes(new SilentField())).thenRun(() -> voidRhapsody.addRecipes(new PureWaterfall())).thenRun(voidRhapsody::composeRecipes);
        return voidRhapsody;
    }

    private CraftItem createDeterminationSymphony() {
        CraftItem determinationSymphony = new CraftItem(SYMPHONY);
        CompletableFuture.runAsync(() -> determinationSymphony.addRecipes(new BlueRoseRainfall())).thenRun(() -> determinationSymphony.addRecipes(new PureWaterfall())).thenRun(determinationSymphony::composeRecipes);
        return determinationSymphony;
    }

    private CraftItem createOperaOfWasteland() {
        CraftItem wasteland = new CraftItem(OPERA);
        CompletableFuture.runAsync(() -> wasteland.addRecipes(new DeathSword())).thenRun(() -> wasteland.addRecipes(new SilentField())).thenRun(() -> wasteland.addRecipes(new FireCrystal())).thenRun(wasteland::composeRecipes);
        return wasteland;
    }

    private CraftItem createFireBird() {
        CraftItem fireBird =  new CraftItem(FIRE);
        CompletableFuture.runAsync(() -> fireBird.addRecipes(new FireCrystal())).thenRun(() -> fireBird.addRecipes(new BirdEggs())).thenRun(fireBird::composeRecipes);
        return fireBird;
    }

    @Override
    public String[] getItemNames() {
        return itemName;
    }

    public String getItemNames(int index) {
        return itemName[index];
    }

    @Override
    public List<CraftItem> findAll() {
        return allItems;
    }
}
