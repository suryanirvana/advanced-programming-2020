package id.ac.ui.cs.tutorial5.core;

import id.ac.ui.cs.tutorial5.service.RandomizerService;

public class SilentField extends Craftable {

    public SilentField() {
        super("Silent Field", RandomizerService.getRandomCostValue());
    }

    @Override
    public String getDoneCraftMessage() {
        return "Empty field, empty place. Nothing here matters. Now ready to mix with other recipes to form new world \n";
    }
}
