package id.ac.ui.cs.tutorial5.core;

import java.util.logging.Logger;

public abstract class Craftable {

    private static final Logger LOGGER = Logger.getLogger(Craftable.class.getName());

    private String name;
    private long stepCost;

    public Craftable(String name, long stepCost) {
        this.name = name;
        this.stepCost = stepCost;
    }

    public String craft() {
        StringBuilder craftResult = new StringBuilder();
        craftResult.append(startCraft());
        runCraft();
        craftResult.append(getDoneCraftMessage());
        return craftResult.toString();
    }

    public String startCraft() {
        return String.format("Starting to mixing %s. %n", name);
    }

    public void runCraft() {
        try {
            LOGGER.info(String.format("Simulating crafting by thread %s", Thread.currentThread().getName()));
            Thread.sleep(stepCost);
        } catch (InterruptedException ex) {
            LOGGER.warning(String.format("Thread %s is interrupted", Thread.currentThread().getName()));
            Thread.currentThread().interrupt();
        }
    }

    public abstract String getDoneCraftMessage();
}
