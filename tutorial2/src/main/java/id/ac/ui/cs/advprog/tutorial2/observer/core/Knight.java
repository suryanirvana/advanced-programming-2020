package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class Knight extends Adventurer{
    public Knight(String name, Guild guild) {
        super(name, guild);
    }

    @Override
    public void update() {
        if (this.name.equalsIgnoreCase("Knight")) {
            this.getQuests().add(this.guild.getQuest());
        }
    }
}