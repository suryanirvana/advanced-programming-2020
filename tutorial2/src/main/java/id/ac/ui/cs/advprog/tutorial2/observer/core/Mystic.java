package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class Mystic extends Adventurer {
    public Mystic(String name, Guild guild) {
        super(name, guild);
    }

    @Override
    public void update() {
        if (this.name.equalsIgnoreCase("Mystic") && (this.guild.getQuestType().equalsIgnoreCase("D") || this.guild.getQuestType().equalsIgnoreCase("E"))) {
            this.getQuests().add(this.guild.getQuest());
        }
    }
}
