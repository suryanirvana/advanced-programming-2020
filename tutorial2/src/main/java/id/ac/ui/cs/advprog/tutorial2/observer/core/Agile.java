package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class Agile extends Adventurer{
    public Agile(String name, Guild guild) {
        super(name, guild);
    }

    @Override
    public void update() {
        if (this.name.equalsIgnoreCase("Agile") && (this.guild.getQuestType().equalsIgnoreCase("D") || this.guild.getQuestType().equalsIgnoreCase("R"))) {
            this.getQuests().add(this.guild.getQuest());
        }
    }
}
