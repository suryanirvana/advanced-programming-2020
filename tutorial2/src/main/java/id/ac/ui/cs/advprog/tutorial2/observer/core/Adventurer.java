package id.ac.ui.cs.advprog.tutorial2.observer.core;

import java.util.ArrayList;
import java.util.List;

public abstract class Adventurer {
        protected Guild guild;
        protected String name;
        private List<Quest> quests = new ArrayList<>();

        public Adventurer(String name, Guild guild) {
                this.name = name;
                this.guild = guild;
                this.guild.add(this);

        }

        public abstract void update();

        public String getName() {
                return this.name;
        }

        public List<Quest> getQuests() {
                return this.quests;
        }
}
